import { Observable, Subscriber } from 'rxjs/Rx';
import * as fs from 'fs';
import * as node_path from 'path';

export class FileSystem {

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    /**
     * Delete the given directory.
     * If the directory is not empty then an error will be thrown
     * unless allowNonEmpty is true.
     * 
     * dir must be an absolute path (but it can contain relative parts).
     * The path will be normalized for the current OS (Windows or Linux).
     * 
     * Each file that is deleted will result in subscriber.next() being called.
     * subscribe.complete() will be called when all files are deleted.
     */
    public deleteDir(dir: string, allowNonEmpty: boolean): Observable<{total: number, current: number, file: string}> {

        dir = this.normalizePath(dir, true);

        let deleteObservables: Observable<void>[] = [];
        let total = 0;
        let current = 0;

        let createObservableFn = (current: number, path: string, isFile: boolean) => {
            
            let observable = isFile ? this.unlinkFile(path) : this.rmDir(path);
            
            return observable.map(() => {
                return { total: total, current: current, file: path };
            });
        };

        return this.getDirectoryContents(dir, allowNonEmpty).concatMap(contents => {

            if (allowNonEmpty !== true && contents.length > 0) {
                throw new Error(`${dir} is not empty. Set "allowNonEmpty" to true to allow non-empty directories to be deleted.`);
            }

            if(contents.length >  1) {
                current = 1;
                total = contents.length;
            }

            contents.forEach(a => {
                let deleteObservable: Observable<any> = createObservableFn(current++, a.path, a.isFile);
                deleteObservables.push(deleteObservable);
            });

            return <Observable<{total: number, current: number, file: string}>><any>Observable.concat(deleteObservables).concatAll();
        });
    }



    /**
     * Get the contents of the directory. If the recursive parameter is set then this will
     * get all content in all children. The contents will be returned in an ordered way that 
     * each path can be deleted in the order it is returned.
     * 
     * The path paramater can be a Windows or Linux path and must be absolute 
     * (but it can contain relative parts, e.g, c:\foo\bar\..\baz)      
     */
    public getDirectoryContents(path: string, recursive: boolean): Observable<{ isFile: boolean, path: string }[]> {

        path = this.normalizePath(path, true);

        //--------------------------------------------------
        // Get file stat for current directory
        //--------------------------------------------------
        let currentDirStatsObservable: Observable<fs.Stats> = this.getFileStats(path);

        //--------------------------------------------------
        // Get all the files/folder names in the current directory
        //--------------------------------------------------
        let filesInCurrentDirObservable: Observable<string[]> = currentDirStatsObservable.concatMap(stats => {
            
            if(!stats.isDirectory()) {
                let error = new Error(`${path} is not a directory`);
                throw error;
            }

            return this.readDir(path);
        });

        let sortedFilesObservable: Observable<{ isFile: boolean, path: string }[]> = filesInCurrentDirObservable.concatMap(contents => {

            if(contents.length < 1) {
                return Observable.of([]);
            }

            let retVal: { isFile: boolean, path: string }[] = [];

            //Get the full path of the directory contents
            contents = contents.map(a => this.normalizePath(`${path}/${a}`, false));

            //Create a list of observables where each observable examines one path and adds it to the retVal
            let statsObservables: Observable<void>[] = contents.map(fullPath => {

                return this.getFileStats(fullPath).map(fileStats => {
                    
                    retVal.push({
                        isFile: !fileStats.isDirectory(),
                        path: fullPath
                    });

                    return undefined;
                });
            });

            //Combine the statsObservables so that they all finish so we can return the
            //fully populated values.
            return Observable.combineLatest(statsObservables).map(() => {
                return retVal;
            });
        });

        return sortedFilesObservable.concatMap(contents => {

            let directories = contents.filter(a => !a.isFile);
            let files = contents.filter(a => a.isFile);

            directories.sort((a,b) => a.path.localeCompare(b.path));
            files.sort((a,b) => a.path.localeCompare(b.path));

            //Directories MUST come before files
            let retVal: { isFile: boolean, path: string }[] = [...directories, ...files];

            //If not recursive then we are done
            if (recursive !== true || directories.length < 1) {
                return Observable.of(retVal);
            }

            

            //Need to create a list of observables for the contents of each directory
            let observableList: Observable<void>[] = directories.map(dirs => {
                
                return this.getDirectoryContents(dirs.path, true).map(dirChildren => {

                    //Be sure children come FIRST.
                    retVal = dirChildren.concat(retVal);
                });
            });
 
            return Observable.combineLatest(observableList).map(() => {
                return retVal;
            });
        });
    }



    /**
     * This will create the directory if it does not exits.
     * It will create all parts of the directory tree as needed.
     * 
     * The dir input must be an absolute path (but can contain relative parts)
     * and can be either a Windows or Linux path (it will be normalized as needed.)
     * 
     * If any errors occure the subscriber.error() function will be passed 
     * the original error.
     */
    public makeDir(dir: string): Observable<void> {

        return Observable.create((subscriber: Subscriber<void>) => {

            dir = this.normalizePath(dir, true, '/');
            let dirParts = dir.split('/');

            //Handle root paths on linux
            if(dirParts.length > 0 && dirParts[0].length === 0) {
                dirParts[0] = node_path.sep;
            }
            
            dirParts = dirParts.filter(a => (a || '').length > 0);
            
            let currentDir: string | null = null;
            let statObservables: Observable<void> | null = null;

            //This function provides a protection against the closure
            //in the for loop
            let createStatObservableFn = (dir: string) => {

                return Observable.create((subscriber: Subscriber<void>) => {

                    fs.lstat(dir, (error, stats) => {

                        //An error indicates the file does not exits (at least it does 99.99% of the time, we could examine the error to be sure....)
                        let exists = error ? false : true;

                        //If the file exists then we don't have anything to do.
                        if(exists) {
                            subscriber.next();
                            subscriber.complete();
                            return;
                        }

                        //If the file does not exist we must wait for it to be created
                        fs.mkdir(dir, (error) => {
                            if(error) {
                                subscriber.error(error);
                                return;
                            }

                            subscriber.next();
                            subscriber.complete();
                        });
                    });
                });
            };

            for(let i = 0; i < dirParts.length; i++) {

                if(!currentDir) {
                    currentDir = dirParts[i];
                } else {
                    currentDir = `${currentDir}/${dirParts[i]}`;
                }

                let testDir = this.normalizePath(currentDir, true);
                let terminal = i === (dirParts.length - 1);
                
                let statObservable = createStatObservableFn(testDir);

                statObservables =  statObservables ? statObservables.concat(statObservable) : statObservable;
            }

            if(!statObservables) {
                subscriber.next();
                subscriber.complete();
                return;
            }

            statObservables.subscribe(() => {

            }, error => {

                subscriber.error(error);

            }, () => {
                subscriber.next();
                subscriber.complete();
            });
        });
    }



    /**
     * Normalize the path. The path is expected to be absolute (but relative parts are allowed).
     * 
     * This will remove ".." and ".", e.g., c:\path1\path2\..\foo.txt => c:\path1\foo.txt.
     * 
     * Extraneous slashes will be removed.
     * 
     * Slashes will be converted to OS path separator or as specified by pathSep.
     * pathSep should not be specified when doing OS dependent operations.
     * 
     * If isDir is true than a trailing slash is appended, else trailing slash is removed.
     */
    public normalizePath(path: string, isDir: boolean, pathSep?: string): string {

        pathSep = pathSep || node_path.sep;

        path = path.replace(/(?:\\|\/){1,5}/g, node_path.sep);
        path = node_path.normalize(path);

        let endsWithSlash = path.endsWith('\\') || path.endsWith('/');
        if(isDir && !endsWithSlash) {
            path += pathSep;
        } else if(!isDir && endsWithSlash && path.length > 1) {
            path = path.substr(0, path.length - 1);
        }

        path = path.replace(/(?:\\|\/){1,5}/g, pathSep);

        return path;
    }



    /**
     * The path is expected to be absolute (but relative parts are allowed).
     * 
     * This will remove ".." and ".", e.g., c:\path1\path2\..\foo.txt => c:\path1\foo.txt.
     * 
     * Extraneous slashes will be removed.
     * 
     * Slashes will be converted to OS path separator or as specified by pathSep.
     * pathSep should not be specified when doing OS dependent operations.
     * 
     * If the file does not exist an error will be thrown.
     * 
     * UTF8 encoding is assumed.
     */
    public readFile(file: string): Observable<string> {
        return Observable.create((subscriber: Subscriber<string>) => {

            file = this.normalizePath(file, false);
            fs.readFile(file, 'utf8', (err, data) => {

                if (err) {
                    subscriber.error(err);
                } else {
                    subscriber.next(data);
                    subscriber.complete();
                }
            });
        });
    }



    //====================================================================================================
    // PRIVATE
    //====================================================================================================
    /**
     * Observable version of fs.lstat.
     * path will be normalized (but it must be absolute).
     */
    private getFileStats(path: string): Observable<fs.Stats> {
        return Observable.create((subscriber: Subscriber<fs.Stats>) => {

            path = this.normalizePath(path, false);

            fs.lstat(path, (error, stats) => {

                if(error) {
                    subscriber.error(error);
                    return;
                }

                subscriber.next(stats);
                subscriber.complete();
            });
        });
    }



    /**
     * Observable version of fs.readDir.
     * path will be normalized (but it must be absolute).
     */
    private readDir(path: string): Observable<string[]> {
        return Observable.create((subscriber: Subscriber<string[]>) => {

            path = this.normalizePath(path, true);

            fs.readdir(path, (error, files) => {

                if(error) {
                    subscriber.error(error);
                    return;
                }

                subscriber.next(files || []);
                subscriber.complete();

            });
        });
    }



    /**
     * Observable version of fs.rmdir
     */
    private rmDir(path: string): Observable<void> {
        return Observable.create((subscriber: Subscriber<void>) => {

            fs.rmdir(this.normalizePath(path, false), (error) => {

                if(error) {
                    subscriber.error(error);
                    return;
                }

                subscriber.next();
                subscriber.complete();
            });
        });
    }



    /**
     * Observable version of fs.unlink
     */
    private unlinkFile(path: string): Observable<void> {
        return Observable.create((subscriber: Subscriber<void>) => {

            fs.unlink(this.normalizePath(path, false), (error) => {

                if(error) {
                    subscriber.error(error);
                    return;
                }

                subscriber.next();
                subscriber.complete();
            });
        });
    }
}